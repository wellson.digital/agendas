<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('nome', 100);
            $table->date('datanascimento');
            $table->text('endereco');
            $table->char('telefonefixo', 15)->nullable();
            $table->char('telefonecelular', 15);
            $table->string('email', 200)->nullable();
            $table->string('redesociais', 2000)->nullable();
            $table->string('avatar', 200)->nullable();
            $table->text('nota')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contatos');
    }
}
