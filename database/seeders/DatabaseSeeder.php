<?php

namespace Database\Seeders;

use Faker\Factory;
use App\Models\Contato;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->createContatos();

    }


    private function createContatos()
    {
        $max = rand(10, 100);

        for($i=0; $i < $max; $i++):
            $this->createContato($i);
        endfor;

        $this->command->info($max . ' demo users created');
    }

    private function createContato($index)
    {

        $faker = Factory::create();
        $faker->addProvider($faker);
        return Contato::create([
            'user_id'  =>'1',
            'nome'  => $faker->name,
            'datanascimento'=>$faker->date('Y-m-d'),
            'endereco'=>'Rua '. $index,
            'telefonefixo'=>'(63)3312-436'. $index,
            'telefonecelular'=>'(63)99995-952'. $index,
            'email' => 'email' . $index . '@mail.com',
            'redesociais' => 'www.a' . $index . '.com.br',
        ]);
    }
}
