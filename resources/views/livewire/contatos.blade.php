<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        Agenda Telefônica:
    </h2>
</x-slot>
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
            @if (session()->has('message'))
                <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3" role="alert">
                    <div class="flex">
                        <div>
                            <p class="text-sm">{{ session('message') }}</p>
                        </div>
                    </div>
                </div>
            @endif

            <button wire:click="create()" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-3">Novo contato</button>

            @if($isModal)
                @include('livewire.create')
            @endif

            <!--<div class="flex justify-end w-2/3 pb-12">
                <input class="px-2 text-sm border border-gray-300 rounded shadow" wire:model="busca" type="text" placeholder="Search Product">
            </div>-->

            <input wire:model="busca" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  type="text" placeholder="Buscar Contatos...">
<br>

            <!--<input wire:model="busca" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  type="text" placeholder="Buscar usuarios...">
            -->

<!--<form  wire: submit.prevent = " search (Object.fromEntries (new FormData ($ event.target))) " >
        <input  type=" text "  name= " busca "  : value=" $busca " />
 </form >-->

            <table class="table-fixed w-full">
                <thead>
                    <tr class="bg-gray-100">
                        <th class="px-4 py-2">Nome</th>
                        <th class="px-4 py-2">Informações do contato</th>
                        <th class="px-2 py-2">Ações</th>

                    </tr>
                </thead>
                <tbody>
                    @forelse($arraycontatos as $row)
                        <tr>
                            <td class="border px-4 py-2" wire:click="edit({{ $row->id }})" >{{ $row->nome }}</td>
                            <td class="border px-4 py-2">


                                 @if(empty($row->telefonefixo))

                                 @else
                                 <p>{{ $row->telefonefixo}} - Telefone Fixo</p>
                                 <hr>
                                @endif

                                 <p>{{ $row->telefonecelular }} - Celular</p>
                                 <hr>
                                @if(empty($row->email))

                                 @else
                                 <p>{{ $row->email }} - Email</p>
                                 <hr>
                                @endif

                                @if(empty($row->redesociais))

                                @else
                                <p>{{ $row->redesociais }} - Site / Rede Sociais</p>
                                <hr>
                               @endif




                            </td>
                            <td class="border px-24 py-2">
                                <button wire:click="edit({{ $row->id }})" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Editar</button>
                                <button wire:click="delete({{ $row->id }})" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">Excluir</button>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="border px-4 py-2 text-center" colspan="5">Sem dados disponíveis</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            {{ $arraycontatos->links() }}
        </div>
    </div>
</div>
