<?php

namespace App\Http\Livewire;

use DateTime;
use Livewire\Request;
use App\Models\Contato;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;

class Contatos extends Component
{
    use WithPagination;

    public $contatos, $contato_id,$user_id, $nome, $datanascimento, $endereco, $telefonefixo,$telefonecelular, $email,$redesociais,$avatar,$nota;
    public $isModal = 0;

    public $perPage = 10;
    public $busca = '';

    public $orderBy = 'nome';
    public $orderAsc = true;

    public function render()
    {


        $contatos = !empty($this->busca) ? Contato::where('nome', 'like', '%'.trim($this->busca).'%')->orderBy('nome')->where('user_id','=', Auth::user()->id)->latest()->paginate(10) : Contato::orderBy('nome')->where('user_id','=', Auth::user()->id)->latest()->paginate(10);
        return view('livewire.contatos',['arraycontatos' => $contatos]);

    }




    public function create()
    {

        $this->resetFields();

        $this->openModal();
    }

    public function closeModal()
    {
        $this->isModal = false;
    }


    public function openModal()
    {
        $this->isModal = true;
    }


    public function resetFields()
    {
        $this->user_id = '';
        $this->nome = '';
        $this->datanascimento = '';
        $this->endereco = '';
        $this->telefonefixo = '';
        $this->telefonecelular = '';
        $this->email = '';
        $this->redesociais ='';
        $this->avatar = '';
        $this->contato_id = '';
    }


    public function store()
    {

        $this->validate([

            'nome' => 'required|string|min:4',

            'datanascimento' => 'required',
            'endereco' => 'required',
            'telefonefixo' => 'string',
            'telefonecelular' => 'required|string',
            'email' => 'email|unique:contatos,email,' . $this->contato_id,
        ]);


        Contato::updateOrCreate(
            ['id' => $this->contato_id], [
            'user_id'=> Auth::user()->id,
            'nome' => $this->nome,
             $myDateTime = DateTime::createFromFormat('d/m/Y', $this->datanascimento),
             'datanascimento' => $myDateTime->format('Y-m-d'),
            'endereco' => $this->endereco,
            'telefonefixo' => $this->telefonefixo,
            'telefonecelular' => $this->telefonecelular,
            'email' => $this->email,
            'redesociais' => $this->redesociais,

        ]);


        session()->flash('message', $this->contato_id ? $this->nome . ' Atualizado': $this->nome . ' Adicionado');
        $this->closeModal();
        $this->resetFields();
    }


    public function edit($id)
    {
        $contato = Contato::find($id);

        $this->contato_id = $id;
        $this->user_id = $contato->user_id;
        $this->nome = $contato->nome;

        $this->datanascimento = $contato->datanascimento;
        $myDateTime = DateTime::createFromFormat('Y-m-d', $contato->datanascimento);
        $this->datanascimento = $myDateTime->format('d/m/Y');
        $this->endereco = $contato->endereco;
        $this->telefonefixo = $contato->telefonefixo;
        $this->telefonecelular = $contato->telefonecelular;
        $this->email = $contato->email;
        $this->redesociais = $contato->redesociais;
         $this->openModal();
    }


    public function delete($id)
    {
        $contato = Contato::find($id);
        $contato->delete();
        session()->flash('message', $contato->nome . ' Deletado com Sucesso');
    }
}
