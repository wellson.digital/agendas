# Agenda - Projeto Final 

Wellson Almeida dos santos

Baixe o  projeto git clone git@gitlab.com:wellson.digital/agendas.git
<br>
<p>1 - Copie o arquivo env.example criando um arquivo com nome **.env **</p>
<p>1.1 - Crie novo banco de dados na máquina.</p> 
<p>1.2 - Altere no arquivo .env o nome do banco de dados e coloque login e senha caso tenha.</p>

		DB_CONNECTION=mysql
		DB_HOST=127.0.0.1
		DB_PORT=3306
		DB_DATABASE= **laravel**
		DB_USERNAME=root
		DB_PASSWORD=


2 - Instalando o Jestream - **composer require laravel/jetstream**

3 - Instalando pacotes NPM - **npm install** 

4 - Rodando npm - **npm run dev**

5  - instalando a key - **php artisan key:generate**

6 - Criando o banco - **php artisan migrate**

7 - Publicando vendor publish - **php artisan vendor:publish --tag=jetstream-view**

8 -  acessando o projeto: **php artisan serve**

9 - Criando novo registro clicando no botão **[Novo registro]**, efetuando o cadastro e depois logando com login e senha. 

10 - Populando o banco com Seeder com registro da faker - **php artisan db:seed**

<hr><br>
**Descrição do enunciado**

Proficiência em Laravel

Faça uma aplicação Jetstream para uma agenda telefônica.
Siga as seguintes especificações:


Devemos armazenar o Nome, Endereço Completo, Data de Aniversário e os contatos da pessoa.

1) Cada pessoa pode ter quantos contatos o usuário quiser cadastrar.
2) Cada contato deve ser de um tipo que pode ser parametrizado no sistema, pelo usuário. Por exemplo: Telefone Fixo, Telefone Celular, E-mail,  etc...
3) A tela inicial do sistema mostra a lista dos registros no banco de dados, ordenado alfabeticamente; paginado de 10 em 10 registros. 
   Deverão ser mostrados Nome e os contatos da pessoa.
	Por exemplo:

		Ciclano de Tal     	(xx) xxxxx-xxxx - Celular
					email@site.com.br  - Email
					https://www.facebook.com/fulano/ - Facebook

		Fulano de Tal		(xx) xxxxx-xxxx - Fixo

3.1) nesta tela inicial deve ter um botão para incluir um novo registro

3.2) deverá ser possível pesquisar por nome ou parte dele (3 ou mais letras)
4) Se clicar no nome, vamos para uma tela onde podemos editar o registro.
4.1) Na tela de edição do Registro poderemos acrescentar / alterar ou excluir contatos.
5) Na tela de edição, teremos um botão para deletar aquele registro.

- crie a migration de todas as tabelas e os seeders que forem necessários
- suba a aplicação para um repositório do gitlab (ou github) e envie a URL 
- Envie as instruções para o setup da aplicação.

- Os quesitos avaliados serão:
	Aplicação que funcione como especificado;
	Aderência aos conceitos do Framework Laravel / Jetstream;
	Código conciso e bem documentado;
	Capricho e Criatividade.

