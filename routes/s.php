Teste proficiência em Laravel nível Júnior

Faça uma aplicação Jetstream para uma agenda telefônica.
Siga as seguintes especificações:

Devemos armazenar o Nome, Endereço Completo, Data de Aniversário e os contatos da pessoa.

1) Cada pessoa pode ter quantos contatos o usuário quiser cadastrar.
2) Cada contato deve ser de um tipo que pode ser parametrizado no sistema, pelo usuário. Por exemplo: Telefone Fixo, Telefone Celular, E-mail,  etc...
3) A tela inicial do sistema mostra a lista dos registros no banco de dados, ordenado alfabeticamente; paginado de 10 em 10 registros.
   Deverão ser mostrados Nome e os contatos da pessoa.
	Por exemplo:

		Rodolfo Stein     	(62) 99987-7108 - Celular
					rodolfo@masterix.com.br  - Email
					https://www.facebook.com/rodolfostein/ - Facebook

		Fulano de Tal		(31) 12345-6789 - Fixo

3.1) nesta tela inicial deve ter um botão para incluir um novo registro

3.2) deverá ser possível pesquisar por nome ou parte dele (3 ou mais letras)
4) Se clicar no nome, vamos para uma tela onde podemos editar o registro.
4.1) Na tela de edição do Registro poderemos acrescentar / alterar ou excluir contatos.
5) Na tela de edição, teremos um botão para deletar aquele registro.

- crie a migration de todas as tabelas e os seeders que forem necessários
- suba a aplicação para um repositório do gitlab (ou github) e envie a URL
- Envie as instruções para o setup da aplicação.

- Os quesitos avaliados serão:
	Aplicação que funcione como especificado;
	Aderência aos conceitos do Framework Laravel / Jetstream;
	Código conciso e bem documentado;
	Capricho e Criatividade.



A tela inicial deve conter a lista de contatos  em ordem alfabetica, paginado registro de 10 em 10
mostrando



php artisan make:model Contato -m
php artisan make:livewire Contatos















https://hdtuto.com/article/laravel-8-jetstream-inertia-js-crud-with-tailwind-modal

composer create-project --prefer-dist laravel/laravel agenda
https://www.tutsmake.com/laravel-8-livewire-crud-with-jetstream-tutorial/

https://www.nicesnippets.com/blog/laravel-8-jetstream-livewire-crud-app-example
https://github.com/hafet17/CRUD-Livewire-Jetstream-L8
https://www.itsolutionstuff.com/post/laravel-8-livewire-crud-with-jetstream-tailwind-cssexample.html

https://github.com/angelitocsg/laravel-agenda-contatos

https://github.com/idrissiradi/Laravel-Livewire-CRUD-Application
https://www.webslesson.info/2020/08/how-to-make-crud-application-laravel-7-using-livewire.html
https://github.com/idrissiradi/Laravel-Livewire-CRUD-Application
https://www.nicesnippets.com/blog/laravel-8-jetstream-livewire-crud-app-example

https://github.com/libredesarrollo/Curso-Laravel-Livewire

php artisan make: migration create_agenda_table
https://github.com/schoolofnetcom/livewire-crud

php artisan migrate:fresh - deleta todas tabelas e recria novamente

composer create-project --prefer-dist laravel/laravel agenda


Route::get('agenda',[ShowAgendas::class,'render'])->name("livewire.render");

php artisan make:model Agenda -m
https://dev.to/kingsconsult/customize-laravel-jetstream-registration-and-login-210f

https://www.codecheef.org/article/laravel-livewire-laravel-livewire-crud-tutorial
https://dev.to/kingsconsult/laravel-8-auth-registration-and-login-32jl
https://www.webslesson.info/2020/10/datatables-with-livewire-in-laravel-8.html
https://www.youtube.com/watch?v=MKt6UJrKrR8

https://github.com/davidgrzyb/laravel-livewire-datatable-example
https://github.com/crusherblack/Laravel8-CRUD-Jetstream-Livewire-Tailwind

https://laraveling.tech/conheca-o-laravel-jetstream/
https://dev.to/erikaheidi/creating-a-multi-user-to-do-application-with-laravel-jetstream-2p1k

https://github.com/vmiguellima/laravel-jetstream-livewire
https://daengweb.id/membuat-crud-laravel-8-jetstream-livewire
https://hdtuto.com/article/laravel-8-jetstream-inertia-js-crud-with-tailwind-modal



FUNCIONANDO MUITO BOM
https://daengweb.id/membuat-crud-laravel-8-jetstream-livewire

https://blog.especializati.com.br/seeders-no-laravel/
