<?php

use App\Http\Livewire\Contatos;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('auth.login');
});

Route::group(['middleware' => ['auth:sanctum', 'verified']], function() {
    Route::get('/dashboard', function() {
        return view('dashboard');
    })->name('dashboard');

    Route::get('contato', Contatos::class)->name('contato');
   // Route::get('search', Contatos::class,'search')->name('search');
});
